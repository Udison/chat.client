/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages;

/**
 *
 * @author Admin
 */
public enum SystemResponse {
    
    SUCCESS,
    FAILED,
    REGISTRATION_EXCEPTION,
    AUTHENTICATION_EXCEPTION,
    SENDING_MESSAGE_EXCEPTION,
    
}
