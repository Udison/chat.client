/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.container;

import ru.udison.prepare.onlinechat.client.dto.OnlineUsersDTO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Admin
 */
public class OnlineUsersContainer {

    
    private static OnlineUsersContainer instance;
    private Set<String> onlineUsers;

    private OnlineUsersContainer() {
        this.onlineUsers = new HashSet<>();
    }
    
    public static OnlineUsersContainer getInstance() {
        if (instance == null)
            instance = new OnlineUsersContainer();
        return instance;
    }
    
    public void refreshOnlineUsers(OnlineUsersDTO dto) {
        onlineUsers.addAll(dto.getOnlineUsers());
    }
    
    public Set<String> getUsers() {
        return new HashSet<>(onlineUsers);
    }
    
    public void writeOnline() {
        for (String s : onlineUsers)
            System.out.println(s);
    }

    @Override
    public String toString() {
        return "OnlineUsersContainer{" +
                "onlineUsers=" + onlineUsers +
                '}';
    }
}
