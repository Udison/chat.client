package ru.udison.prepare.onlinechat.client.entity.messages.converters;

import org.codehaus.jackson.map.ObjectMapper;
import ru.udison.prepare.onlinechat.client.dto.OnlineUsersDTO;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

import java.io.IOException;

public class OnlineUsersDTOConverter {

    private final ObjectMapper mapper;

    private OnlineUsersDTOConverter() {
        this.mapper = new ObjectMapper();
    }

    private static class OnlineUsersDTOConverterHolder {
        public static final OnlineUsersDTOConverter ONLINE_USERS_DTO_CONVERTER_HOLDER =
                new OnlineUsersDTOConverter();
    }

    public static OnlineUsersDTOConverter getInstance() {
        return OnlineUsersDTOConverterHolder.ONLINE_USERS_DTO_CONVERTER_HOLDER;
    }

    public String convertOnlineUsersDTOToString(OnlineUsersDTO dto) throws ConvertException {
        try {
            return mapper.writeValueAsString(dto);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }

    public OnlineUsersDTO convertStringToOnlineUsersDTO(String string) throws ConvertException {
        try {
            return mapper.readValue(string, OnlineUsersDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
}
