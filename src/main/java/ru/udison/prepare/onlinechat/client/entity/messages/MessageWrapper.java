/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages;

import lombok.Getter;

import java.util.Objects;

/**
 *
 * @author Admin
 */
@Getter
public class MessageWrapper {

    private Event event;
    private String payLoad;

    public MessageWrapper() {
    }

    public MessageWrapper(Event event, String payLoad) {
        this.event = event;
        this.payLoad = payLoad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageWrapper wrapper = (MessageWrapper) o;
        return event == wrapper.event &&
                Objects.equals(payLoad, wrapper.payLoad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(event, payLoad);
    }

    @Override
    public String toString() {
        return "MessageWrapper{" +
                "event=" + event +
                ", payLoad='" + payLoad + '\'' +
                '}';
    }
}
