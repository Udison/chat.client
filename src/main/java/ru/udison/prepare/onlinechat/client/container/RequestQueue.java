/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.container;

import lombok.Cleanup;
import ru.udison.prepare.onlinechat.client.entity.user.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 *
 * @author Admin
 */
public class RequestQueue implements Runnable {

    
    private static RequestQueue instance;
    private final ArrayBlockingQueue<String> requests;
    
    private RequestQueue(){
        this.requests = new ArrayBlockingQueue<String>(100);
    }
    
    public static RequestQueue getInstance() {
        if (instance == null)
            instance = new RequestQueue();
        return instance;
    }
    
    public void addRequest(String request) {
        requests.add(request);
    }
    
    public String takeRequest() throws InterruptedException {
        return requests.take();
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String request = reader.readLine();
                requests.add(request);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
