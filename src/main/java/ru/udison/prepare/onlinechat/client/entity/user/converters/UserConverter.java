package ru.udison.prepare.onlinechat.client.entity.user.converters;

import org.codehaus.jackson.map.ObjectMapper;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.client.entity.user.User;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

import java.io.IOException;

public class UserConverter {

    private final ObjectMapper mapper;

    public UserConverter() {
        this.mapper = new ObjectMapper();
    }

    public String convertUserToString(User user) throws ConvertException {
        try {
            return mapper.writeValueAsString(user);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }

    public User convertStringToUser(String string) throws ConvertException {
        try {
            return mapper.readValue(string, User.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
}
