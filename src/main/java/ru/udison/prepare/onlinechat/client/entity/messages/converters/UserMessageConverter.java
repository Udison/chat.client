/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages.converters;

import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;
import ru.udison.prepare.onlinechat.client.entity.messages.UserMessage;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

/**
 *
 * @author Admin
 */
public class UserMessageConverter {
        
    private final ObjectMapper mapper;

    public UserMessageConverter() {
        this.mapper = new ObjectMapper();
    }
    
    public String convertMessageToString(UserMessage message) throws ConvertException {
        try {
            return mapper.writeValueAsString(message);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
    
    public UserMessage convertStringToMessage(String string) throws ConvertException {
        try {
            return mapper.readValue(string, UserMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
}
