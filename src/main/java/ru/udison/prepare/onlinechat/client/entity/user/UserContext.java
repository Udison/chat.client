package ru.udison.prepare.onlinechat.client.entity.user;

import lombok.Getter;
import lombok.Setter;
import ru.udison.prepare.onlinechat.client.Connection;

import java.io.*;


public class UserContext {


    private static UserContext instance;
    @Getter @Setter
    private User user;
    @Getter @Setter
    private Connection connection;
    @Getter @Setter
    private boolean isConnected;
    @Getter @Setter
    private boolean isAuthenticated;

    private UserContext() {}

    public static UserContext getInstance() {
        if (instance == null)
            instance = new UserContext();
        return instance;
    }

}
