package ru.udison.prepare.onlinechat.client.messagemanager;


import ru.udison.prepare.onlinechat.client.container.CommandsContainer;
import ru.udison.prepare.onlinechat.client.container.OnlineUsersContainer;
import ru.udison.prepare.onlinechat.client.container.RequestQueue;
import ru.udison.prepare.onlinechat.client.entity.messages.Event;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageToAllUsers;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.client.entity.messages.UserMessage;
import ru.udison.prepare.onlinechat.client.entity.messages.converters.MessageToAllUsersConverter;
import ru.udison.prepare.onlinechat.client.entity.messages.converters.UserMessageConverter;
import ru.udison.prepare.onlinechat.client.entity.messages.handlers.AuthenticationHandler;
import ru.udison.prepare.onlinechat.client.entity.messages.handlers.RegistrationHandler;
import ru.udison.prepare.onlinechat.client.entity.messages.sender.MessageSender;
import ru.udison.prepare.onlinechat.client.entity.user.UserContext;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

public class RequestManager implements Runnable {


    public RequestManager() {

    }


    @Override
    public void run() {
        while (true) {
            try {
                String request = RequestQueue.getInstance().takeRequest();
                handleMessage(request);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void handleMessage(String request) {
        String[] values = request.split(" ");
        if (CommandsContainer.getInstance().getCommands().contains(values[0])) {
            useCommand(values);
            return;
        } else if (UserContext.getInstance().isAuthenticated()) {
            MessageToAllUsers messageToAllUsers = new MessageToAllUsers(UserContext
                    .getInstance()
                    .getUser()
                    .getNickName(), request);
            try {
                MessageSender.getInstance().sendMessage(new MessageWrapper(
                        Event.TOALLUSERs, new MessageToAllUsersConverter().convertMessageToString(messageToAllUsers)));
            } catch (ConvertException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Sign in to send message");
            CommandsContainer.getInstance().getHelp();
        }
    }

    private void useCommand(String[] values) {
        switch (values[0]) {
            case "/h":
            case "/help":
                CommandsContainer.getInstance().getHelp();
                break;
            case "/in":
            case "/signIn":
                new AuthenticationHandler().run();
                break;
            case "/up":
            case "/signUp":
                new RegistrationHandler().run();
                break;
            case "/pm":
                if (!UserContext.getInstance().isAuthenticated()) {
                    System.out.println("Sign in to send message");
                    break;
                }
                if (!checkOnline(values[1])) {
                    System.out.println("User with such nickname is not found or is offline");
                    break;
                }
                UserMessage message = new UserMessage(UserContext
                        .getInstance()
                        .getUser()
                        .getNickName(), values[1], values[2]);
                try {
                    MessageSender.getInstance().sendMessage(new MessageWrapper(
                            Event.TOUSER, new UserMessageConverter().convertMessageToString(message)));
                    break;
                } catch (ConvertException e) {
                    e.printStackTrace();
                    break;
                }
            case "/q":
            case "/quit":
                MessageSender.getInstance().sendMessage(new MessageWrapper(Event.QUIT, ""));
                break;
            case "/online":
                OnlineUsersContainer.getInstance().writeOnline();
                break;
            case "/refresh.online":
                MessageSender.getInstance().sendMessage(new MessageWrapper(Event.GIVEONLINE, ""));
                break;
        }
    }

    private boolean checkOnline(String nickname) {
        return OnlineUsersContainer
                .getInstance()
                .getUsers()
                .contains(nickname);
    }


}
