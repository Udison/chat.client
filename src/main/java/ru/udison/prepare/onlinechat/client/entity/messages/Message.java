/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages;

import java.time.LocalDateTime;

/**
 *
 * @author Admin
 */
public class Message {
    
    private LocalDateTime sendTime;



    public Message() {

    }

    public Message(LocalDateTime sendTime) {
        this.sendTime = sendTime;
    }

    public LocalDateTime getSendTime() {
        return sendTime;
    }
    
}
