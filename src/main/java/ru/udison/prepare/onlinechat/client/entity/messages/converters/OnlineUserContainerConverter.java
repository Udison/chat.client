package ru.udison.prepare.onlinechat.client.entity.messages.converters;

import org.codehaus.jackson.map.ObjectMapper;
import ru.udison.prepare.onlinechat.client.container.OnlineUsersContainer;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

import java.io.IOException;

public class OnlineUserContainerConverter {

    private final ObjectMapper mapper;

    public OnlineUserContainerConverter() {
        this.mapper = new ObjectMapper();
    }

    public String convertContainerToString(OnlineUsersContainer container) throws ConvertException {
        try {
            return mapper.writeValueAsString(container);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }

    public OnlineUsersContainer convertStringToContainer(String string) throws ConvertException {
        try {
            return mapper.readValue(string, OnlineUsersContainer.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
}
