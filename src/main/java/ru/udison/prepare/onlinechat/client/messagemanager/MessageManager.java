/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.messagemanager;


import ru.udison.prepare.onlinechat.client.entity.messages.MessageWrapper;

/**
 *
 * @author Admin
 */
public interface MessageManager {

    
    void handleMessage(MessageWrapper wrapper);
    
}
