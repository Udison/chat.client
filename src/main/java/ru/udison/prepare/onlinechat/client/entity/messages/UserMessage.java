/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages;


import java.util.Objects;

/**
 *
 * @author Admin
 */
public class UserMessage extends Message {
    
    private String senderNickname;
    private String recipientNickname;
    private String text;

    public UserMessage() {
    }

    public UserMessage(String senderNickname, String recipientNickname, String text) {
        this.senderNickname = senderNickname;
        this.recipientNickname = recipientNickname;
        this.text = text;
    }


    public String getSenderNickname() {
        return senderNickname;
    }

    public String getRecipientNickname() {
        return recipientNickname;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMessage that = (UserMessage) o;
        return Objects.equals(senderNickname, that.senderNickname) &&
                Objects.equals(recipientNickname, that.recipientNickname) &&
                Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(senderNickname, recipientNickname, text);
    }
}
