/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages.converters;

import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;


/**
 *
 * @author Admin
 */
public class MessageWrapperConverter {
    
    private final ObjectMapper mapper;

    public MessageWrapperConverter() {
        this.mapper = new ObjectMapper();
    }
    
    public String convertWrapperToString(MessageWrapper wrapper) throws ConvertException {
        try {
            return mapper.writeValueAsString(wrapper);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
    
    public MessageWrapper convertStringToWrapper(String string) throws ConvertException {
        try {
            return mapper.readValue(string, MessageWrapper.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
    
}
