/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client;

import ru.udison.prepare.onlinechat.client.container.CommandsContainer;
import ru.udison.prepare.onlinechat.client.container.RequestQueue;
import ru.udison.prepare.onlinechat.client.container.ResponseQueue;
import ru.udison.prepare.onlinechat.client.entity.user.UserContext;
import ru.udison.prepare.onlinechat.client.messagemanager.RequestManager;
import ru.udison.prepare.onlinechat.client.messagemanager.ResponseManager;

import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Admin
 */
public class Connection {

    
    private static Connection instance;
    private Socket socket;

    Connection() {
    }
    
    public static Connection getInstance() {
        if (instance == null)
            instance = new Connection();
        return instance;
    }
    
    public void startConnection(String hostName, int port) throws IOException {
        socket = new Socket(hostName, port);
        UserContext.getInstance().setConnected(true);
        UserContext.getInstance().setConnection(this);
        new Thread(new RequestManager()).start();
        new Thread(ResponseQueue.getInstance()).start();
        new Thread(new ResponseManager()).start();
    }

    public Socket getSocket() {
        return socket;
    }

    public void closeConnection() {
        try {
            socket.shutdownOutput();
            socket.getInputStream();
            socket.close();
            UserContext.getInstance().setConnected(false);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
