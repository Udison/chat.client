/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages;

import java.util.Objects;

/**
 *
 * @author Admin
 */
public class RegistrationMessage extends Message {

    private String ipSender;
    private String nickName;
    private String login;
    private String password;
    private String confirmPassword;

    public RegistrationMessage() {
    }

    public RegistrationMessage(String nickName, String login, String password, String confirmPassword) {
        
        this.nickName = nickName;
        this.login = login;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getIpSender() {
        return ipSender;
    }
    
    public String getNickName() {
        return nickName;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setIpSender(String ipSender) {
        this.ipSender = ipSender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistrationMessage message = (RegistrationMessage) o;
        return Objects.equals(nickName, message.nickName) &&
                Objects.equals(login, message.login) &&
                Objects.equals(password, message.password) &&
                Objects.equals(confirmPassword, message.confirmPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickName, login, password, confirmPassword);
    }
}
