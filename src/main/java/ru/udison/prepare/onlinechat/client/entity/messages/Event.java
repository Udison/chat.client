/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages;

/**
 *
 * @author Admin
 */
public enum Event {

    AUTHENTICATION_SUCCESS,
    AUTHENTICATION_FAILED,
    AUTHENTICATION,
    REGISTRATION_SUCCESS,
    REGISTRATION_FAILED,
    REGISTRATION,
    TOUSER,
    TOALLUSERs,
    QUIT,
    PING,
    PONG,
    GETONLINE,
    GIVEONLINE,
    DISCONECTED,
    EXCEPTION,
    FROMSYSTEM

}
