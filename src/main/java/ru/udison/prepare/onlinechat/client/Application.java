/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client;

import ru.udison.prepare.onlinechat.client.container.CommandsContainer;
import ru.udison.prepare.onlinechat.client.container.RequestQueue;
import ru.udison.prepare.onlinechat.client.messagemanager.RequestManager;

import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Admin
 */
public class Application {
    
    public static void main(String[] args) throws IOException {
        CommandsContainer.getInstance().getHelp();
        Thread readerFromKeyboard = new Thread(RequestQueue.getInstance());
        readerFromKeyboard.start();
        Connection.getInstance().startConnection("127.0.0.1", 8080);
        try {
            readerFromKeyboard.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
}
