/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.container;

import ru.udison.prepare.onlinechat.client.entity.user.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 *
 * @author Admin
 */
public class ResponseQueue implements Runnable {

    
    private static ResponseQueue instance;
    private final ArrayBlockingQueue<String> responses;

    private ResponseQueue() {
        this.responses = new ArrayBlockingQueue<String>(100);
    }
    
    public static ResponseQueue getInstance() {
        if (instance == null)
            instance = new ResponseQueue();
        return instance;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(UserContext
                    .getInstance()
                    .getConnection()
                    .getSocket()
                    .getInputStream()));
            while (UserContext.getInstance().isConnected()) {
                String response = reader.readLine();
                if (response != null) {
                    responses.add(response);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String takeResponse() throws InterruptedException {
        return responses.take();
    }

}