package ru.udison.prepare.onlinechat.client.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

public class OnlineUsersDTO {

    @Getter @Setter
    private Set<String> onlineUsers = new HashSet<>();

    public OnlineUsersDTO() {
    }

    public OnlineUsersDTO(Set<String> onlineUsers) {
        this.onlineUsers = onlineUsers;
    }

    public Set<String> getOnlineUsers() {
        return onlineUsers;
    }
}
