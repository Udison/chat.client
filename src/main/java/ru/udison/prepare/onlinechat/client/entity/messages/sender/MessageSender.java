/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages.sender;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import ru.udison.prepare.onlinechat.client.Connection;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.client.entity.messages.converters.MessageWrapperConverter;
import ru.udison.prepare.onlinechat.client.entity.user.UserContext;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

/**
 *
 * @author Admin
 */
public class MessageSender {

    private static MessageSender instance;

    private MessageSender() {
    }

    public static MessageSender getInstance() {
        if (instance == null) {
            instance = new MessageSender();
        }
        return instance;
    }

    public void sendMessage(MessageWrapper wrapper) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(Connection
                    .getInstance()
                    .getSocket()
                    .getOutputStream()));
            String jsonToSend = new MessageWrapperConverter().convertWrapperToString(wrapper);
            writer.write(jsonToSend + "\r\n");
            writer.flush();
        } catch (ConvertException | IOException e) {
            e.printStackTrace();
        }
    }

}
