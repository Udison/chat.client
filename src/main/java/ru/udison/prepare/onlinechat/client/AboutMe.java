/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client;

import lombok.Getter;

import java.time.LocalDateTime;

/**
 *
 * @author Admin
 */
public class AboutMe {

    private String nickname;
    private String login;
    private String password;

    private LocalDateTime registrationDate;

    public AboutMe(AboutMe me) {
        this.nickname = me.getNickname();
        this.login = me.getLogin();
        this.password = me.getPassword();
        this.registrationDate = me.getRegistrationDate();
    }

    public String getNickname() {
        return nickname;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }
}
