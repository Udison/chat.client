/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.messagemanager;


import ru.udison.prepare.onlinechat.client.container.OnlineUsersContainer;
import ru.udison.prepare.onlinechat.client.container.ResponseQueue;
import ru.udison.prepare.onlinechat.client.dto.OnlineUsersDTO;
import ru.udison.prepare.onlinechat.client.entity.messages.Event;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageToAllUsers;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.client.entity.messages.UserMessage;
import ru.udison.prepare.onlinechat.client.entity.messages.converters.MessageToAllUsersConverter;
import ru.udison.prepare.onlinechat.client.entity.messages.converters.MessageWrapperConverter;
import ru.udison.prepare.onlinechat.client.entity.messages.converters.OnlineUsersDTOConverter;
import ru.udison.prepare.onlinechat.client.entity.messages.converters.UserMessageConverter;
import ru.udison.prepare.onlinechat.client.entity.messages.sender.MessageSender;
import ru.udison.prepare.onlinechat.client.entity.user.User;
import ru.udison.prepare.onlinechat.client.entity.user.UserContext;
import ru.udison.prepare.onlinechat.client.entity.user.converters.UserConverter;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

/**
 * @author Admin
 */
public class ResponseManager implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                String response = ResponseQueue.getInstance().takeResponse();
                handleMessage(response);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void handleMessage(String response) {
        try {
            MessageWrapper message = new MessageWrapperConverter().convertStringToWrapper(response);
            switch (message.getEvent()) {
                case AUTHENTICATION_SUCCESS:
                    User authenticatedUser = new UserConverter().convertStringToUser(message.getPayLoad());
                    UserContext.getInstance().setUser(authenticatedUser);
                    UserContext.getInstance().setAuthenticated(true);
                    System.out.println("Welcome " + authenticatedUser.getNickName() + "! Enjoy the chat");
                    break;
                case REGISTRATION_SUCCESS:
                    User registeredUser = new UserConverter().convertStringToUser(message.getPayLoad());
                    UserContext.getInstance().setUser(registeredUser);
                    System.out.println(registeredUser.toString());
                    break;
                case GETONLINE:
                    OnlineUsersDTO dto = OnlineUsersDTOConverter
                            .getInstance()
                            .convertStringToOnlineUsersDTO(message.getPayLoad());
                    OnlineUsersContainer
                            .getInstance()
                            .refreshOnlineUsers(dto);
                    System.out.println(OnlineUsersContainer.getInstance());
                    break;
                case PING:
                    MessageSender.getInstance().sendMessage(new MessageWrapper(Event.PONG, ""));
                    MessageSender.getInstance().sendMessage(new MessageWrapper(Event.GIVEONLINE, ""));
                    break;
                case TOALLUSERs:
                    MessageToAllUsers m = new MessageToAllUsersConverter().convertStringToMessage(message.getPayLoad());
                    System.out.println(m.getSenderNickname() + ": " + m.getText());
                    break;
                case TOUSER:
                    UserMessage um = new UserMessageConverter().convertStringToMessage(message.getPayLoad());
                    System.out.println(String.format(">>>>> %s: %s", um.getSenderNickname(), um.getText()));
                    break;
                case FROMSYSTEM:
                case AUTHENTICATION_FAILED:
                case REGISTRATION_FAILED:
                    System.out.println(message.getPayLoad());
                    break;
                case DISCONECTED:
                    UserContext.getInstance().setConnected(false);
                    System.out.println(message.getPayLoad());
                    break;

            }
        } catch (ConvertException e) {
            e.printStackTrace();
        }
    }

}
