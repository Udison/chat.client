/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages.handlers;

import ru.udison.prepare.onlinechat.client.container.RequestQueue;
import ru.udison.prepare.onlinechat.client.entity.messages.AuthenticationMessage;
import ru.udison.prepare.onlinechat.client.entity.messages.Event;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.client.entity.messages.converters.AuthenticationMessageConverter;
import ru.udison.prepare.onlinechat.client.entity.messages.sender.MessageSender;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

/**
 *
 * @author Admin
 */
public class AuthenticationHandler {

    private String login;
    private String password;

    private int attemptsToSend;
    private int attemptsToRegistration;

    public AuthenticationHandler() {
    }

    public void run() {

        login = receiveLogin();
        password = receivePassword();
        sendAuthenticationMessage();

    }

    private String receiveLogin() {
        System.out.print("Enter your login: ");
        String login = null;
        try {
            login = RequestQueue.getInstance().takeRequest();
            if (!checkLogin(login)) {
                System.out.println("Wrong login.");
                receiveLogin();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return login;
    }

    private String receivePassword() {
        System.out.print("Enter your password: ");
        String password = null;
        try {
            password = RequestQueue.getInstance().takeRequest();
            if (!checkPass(password)) {
                System.out.println("Wrong password.");
                receivePassword();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return password;
    }

    private void sendAuthenticationMessage() {
        try {
            MessageSender.getInstance().sendMessage(
                    new MessageWrapper(
                            Event.AUTHENTICATION,
                            new AuthenticationMessageConverter()
                            .convertMessageToString(
                                    new AuthenticationMessage(login, password))));
        } catch (ConvertException e) {
            if (attemptsToSend < 2) {
                sendAuthenticationMessage();
                attemptsToSend++;
            }
            if (attemptsToRegistration < 2) {
                attemptsToSend = 0;
                System.out.println("Oops! Something is wrong, try again");
                attemptsToRegistration++;
                run();
            }
            else
                System.out.println("We have some problems, contact support");

        }
    }

    private boolean checkLogin(String login) {
        return !login.matches("\\W") || login.length() > 16;
    }

    private boolean checkPass(String password) {
        return !password.matches("\\W") || password.length() > 25;
    }

}
