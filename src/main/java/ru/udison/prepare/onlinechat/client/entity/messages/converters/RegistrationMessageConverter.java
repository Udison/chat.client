/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages.converters;

import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;
import ru.udison.prepare.onlinechat.client.entity.messages.RegistrationMessage;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

/**
 *
 * @author Admin
 */
public class RegistrationMessageConverter {
        
    private final ObjectMapper mapper;

    public RegistrationMessageConverter() {
        this.mapper = new ObjectMapper();
    }
    
    public String convertMessageToString(RegistrationMessage message) throws ConvertException {
        try {
            return mapper.writeValueAsString(message);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
    
    public RegistrationMessage convertStringToMessage(String string) throws ConvertException {
        try {
            return mapper.readValue(string, RegistrationMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
}
