/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.container;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Admin
 */
public class CommandsContainer {

    private static CommandsContainer instance;
    private List<String> commands;

    public CommandsContainer() {
        commands = Arrays.asList("/h", "help", "/in", "/signIn", "/up", "/signUp", "/pm", "/q", "/quit", "/online", "/refresh.online");
    }
    
    public static CommandsContainer getInstance() {
        if (instance == null) 
            return new CommandsContainer();
        return instance;
    }
    
    public List<String> getCommands() {
        return new ArrayList<>(commands);
    }

    public void getHelp() {
        System.out.println(String.format("%s, %s - Enter to show help", commands.get(0), commands.get(1)));
        System.out.println(String.format("%s, %s - If you have an account, enter to sign in", commands.get(2), commands.get(3)));
        System.out.println(String.format("%s, %s - Enter to registration", commands.get(4), commands.get(5)));
        System.out.println(String.format("%s - To send a private message, enter this command," +
                " then the user’s nickname then your message. Example (/pm Recipient_Nickname Hello!)", commands.get(6)));
        System.out.println(String.format("%s, %s - Enter to quit", commands.get(7), commands.get(8)));
        System.out.println(String.format("%s - Enter to show online users", commands.get(9)));
        System.out.println(String.format("%s - Enter to refresh online users", commands.get(10)));


    }

}
