/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages.handlers;

import ru.udison.prepare.onlinechat.client.container.RequestQueue;
import ru.udison.prepare.onlinechat.client.entity.messages.Event;
import ru.udison.prepare.onlinechat.client.entity.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.client.entity.messages.RegistrationMessage;
import ru.udison.prepare.onlinechat.client.entity.messages.converters.RegistrationMessageConverter;
import ru.udison.prepare.onlinechat.client.entity.messages.sender.MessageSender;
import ru.udison.prepare.onlinechat.client.exceptions.ConvertException;

/**
 *
 * @author Admin
 */
public class RegistrationHandler {

    private String nickname;
    private String login;
    private String password;
    private String confirmedPassword;

    private int attemptsToSend;
    private int attemptsToRegistration;

    public RegistrationHandler() {
    }

    public void run() {

            nickname = receiveNickname();
            login = receiveLogin();
            password = receivePassword();
            confirmedPassword = receiveConfirmPassword(password);
            sendRegistrationMessage();

    }

    private String receiveNickname() {
        String nickname = null;
        System.out.print("Enter your Nickname: ");
        try {
            nickname = RequestQueue.getInstance().takeRequest();
            if (!checkNickname(nickname)) {
                System.out.println("Wrong nickname");
                receiveNickname();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return nickname;

    }

    private String receiveLogin() {
        String login = null;
        System.out.print("Enter your login: ");
        try {
            login = RequestQueue.getInstance().takeRequest();
            if (!checkLogin(login)) {
                System.out.println("Wrong login");
                receiveLogin();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return login;
    }

    private String receivePassword() {
        String password = null;
        System.out.print("Enter your password: ");
        try {
            password = RequestQueue.getInstance().takeRequest();
            if (!checkPass(password)) {
                System.out.println("Wrong password");
                receivePassword();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return password;
    }

    private String receiveConfirmPassword(String password) {
        System.out.print("Confirm password: ");
        String confirmedPass = null;
        try {
            confirmedPass = RequestQueue.getInstance().takeRequest();
            if (!checkConfirmedPass(password, confirmedPassword)) {
                System.out.println("Password is different");
                this.password = receivePassword();
                receiveConfirmPassword(this.password);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return  confirmedPass;

    }

    private void sendRegistrationMessage() {
        try {
            MessageSender.getInstance().sendMessage(
                    new MessageWrapper(
                            Event.REGISTRATION,
                            new RegistrationMessageConverter()
                            .convertMessageToString(
                                    new RegistrationMessage(nickname, login, password, confirmedPassword))));
        } catch (ConvertException e) {
            if (attemptsToSend < 2) {
                sendRegistrationMessage();
                attemptsToSend++;
            }
            if (attemptsToRegistration < 2) {
                attemptsToSend = 0;
                System.out.println("Oops! Something is wrong, try again");
                attemptsToRegistration++;
                run();
            }
            else
                System.out.println("We have some problems, contact support");

        }
    }

    private boolean checkNickname(String nickname) {
        return !nickname.matches("\\W") || nickname.length() > 25;
    }

    private boolean checkLogin(String login) {
        return !login.matches("\\W") || login.length() > 16;
    }

    private boolean checkPass(String password) {
        return !password.matches("\\W") || password.length() > 25;
    }

    private boolean checkConfirmedPass(String password, String confirmedPassword) {
        return password.equals(confirmedPassword);
    }

}
