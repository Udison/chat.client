/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.client.entity.messages;


/**
 *
 * @author Admin
 */
public class SystemMessage extends Message{
    
    private String recipientIp;
    private String response;

    public SystemMessage() {
    }

    public SystemMessage(String ipRecipient, String response) {
        this.recipientIp = ipRecipient;
        this.response = response;
    }

    public String getRecipientIp() {
        return recipientIp;
    }

    public String getResponse() {
        return response;
    }
    
    
    
}
